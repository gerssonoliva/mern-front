import { useState, useEffect, createContext } from 'react';
import { User, Auth } from "../api";
import { hasExpiredToken } from "../utils"

const userController = new User();
const authController = new Auth();

export const AuthContext = createContext();

export function AuthProvider(props) {

  const { children } = props;
  const [user, setUser] = useState(null);
  const [token, setToken] = useState(null);
  const [loading, setLoading] = useState(true); // no mostrar el login mientras se inicia secion

  useEffect(() => {
    // Comprobar si el usuario está logueado o no
    (async () => {
      const accessToken = authController.getAccessToken();
      const refreshToken = authController.getRefreshToken();

      if(!accessToken && !refreshToken){
        logout();
        setLoading(false); // quitar loading
        return; // si alguno de los 2 no existe, nunca entrará sesion
      }
      
      if(hasExpiredToken(accessToken)) {
        if(hasExpiredToken(refreshToken)) {
          logout();
        }else{
          await reLogin(refreshToken); // hacer login nuevamente
        }
      }else{
        await login(accessToken); // HACE LOGIN
      }
      setLoading(false); // quitar loading
      
    })();
  }, []);

  const reLogin = async (refreshToken) => {
    try {
      const { accessToken } = await authController.refreshAccessToken(refreshToken);
      authController.setAccessToken(accessToken);
      await login(accessToken); // login con el nuevo accessToken
    } catch (error) {
      console.log(error);
    }
  }

  const login = async (accessToken) => {
    try {
      const response = await userController.getMe(accessToken);
      delete response.password;
      //console.log(response);
      setUser(response)
      setToken(accessToken);
    } catch (error) {
      console.log(error);
    }
  };

  const logout = () => {
    setUser(null);
    setToken(null);
    authController.removeTokens();
  }

  const data = {
    accessToken: token,
    user,
    login,
    logout
  }

  if(loading) return null;

  return <AuthContext.Provider value={data}>{children}</AuthContext.Provider>
}
