import { image } from "../../../assets";

export const reviewsData = [
    {
      userName: "Alonso Campos",
      userType: "Frontend Developer",
      avatar: image.avatar01,
      comment:
        "Un curso excelente, el profesor explica detalladamente como funciona react native.",
    },
    {
      userName: "Valentina Rubio",
      userType: "FullStack Developer",
      avatar: image.avatar02,
      comment:
        "El contenido del curso es muy completo y de necesitar cualquier dato adicional el profesor se toma su tiempo para explicarte.",
    },
    {
      userName: "David Ramiro",
      userType: "Backend Developer",
      avatar: image.avatar03,
      comment:
        "Si te gustan los cursos que profundizan en la materia, te lo recomiendo. El profesor explica muy bien.",
    },
    {
      userName: "Marc Perez",
      userType: "UI/UX",
      avatar: image.avatar04,
      comment:
        "Empecé el curso sin saber nada de React Native y creo que lo finalizo teniendo un nivel alto.",
    },
    {
      userName: "Critina Perea",
      userType: "Frontend Developer",
      avatar: image.avatar05,
      comment:
        "Todo muy entendible, solo pienso que aún falta actualizar algunos puntos.",
    },
    {
      userName: "Miguel Carranza",
      userType: "Backend Developer",
      avatar: image.avatar06,
      comment:
        "Me a servido de mucho este el curso de Kotlin, lo recomiendo.",
    },
  ];