import React, { useEffect, useState } from 'react';
import { Loader } from "semantic-ui-react";
import { size, map } from "lodash";
import { User } from "../../../../api/user";
import { useAuth } from "../../../../hooks";
import { UserItem } from "../UserItem";

const userController = new User();

export function ListUsers(props) {

  const { usersActive, reload, onReload } = props;
  const [ users, setUsers ] = useState(null);
  const { accessToken } = useAuth();
  
  useEffect(() => {
    (async () => {
      setUsers(null);
      try {
        const response = await userController.getUsers(accessToken, usersActive);
        setUsers(response);
      } catch (error) {
        console.log(error);
      }
    })()
  }, [usersActive, reload]);

  if(!users){
    return <Loader active inline="centered" />
  }

  if(size(users)===0){
    return "No hay ningún usuario."
  }

  return map(users, (user) => <UserItem key={user._id} user={user} onReload={onReload} />)
  /* return (
    <div>
        <h2>Estamos viendo los usuarios</h2>
        <p>{usersActive ? "Activos" : "Inactivos"}</p>
    </div>
  ) */
}
