export const socialData = [
  {
    type: "youtube",
    link: "https://www.youtube.com/channel/UCdh3l0-ikhD8sl_hKC-QFtA"
  },
  {
    type: "twitter",
    link: "https://twitter.com/gerssonoliva"
  },
  {
    type: "instagram",
    link: "https://www.instagram.com/gerson_oliva_r/"
  },
  {
    type: "facebook",
    link: "https://www.facebook.com/gerson.oliva.remigio/"
  },
  {
    type: "linkedin",
    link: "https://www.linkedin.com/in/gerson-oliva-remigio-945428108/"
  }    
];