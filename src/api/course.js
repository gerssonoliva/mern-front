import { ENV } from "../utils";

export class Course {
  baseApi = ENV.BASE_API;
  
  async createCourse(accesToken, data) {
    try {
      const formData = new FormData();
      Object.keys(data).forEach((key) => {
        formData.append(key, data[key])
      });

      if(data.file){
        formData.append("miniature", data.file);
      }

      const url = `${this.baseApi}/${ENV.API_ROUTES.COURSE}`;
      const params = {
        method: "POST",
        headers: {
          Autorization: `Bearer ${accesToken}`
        },
        body: formData
      }

      const response = await fetch(url, params);
      const result = await response.json();

      if(response.status !== 201) throw result;

      return result; 

    } catch (error) {
      throw error;
    }
  }

  async getCourses(params) {
    try {
      const pageFilter = `page=${params?.page || 1}`;
      const limitFilter = `limit=${params?.limit || 10}`;
      const url = `${this.baseApi}/${ENV.API_ROUTES.COURSES}?${pageFilter}&${limitFilter}`;

      const response = await fetch(url);
      const result = await response.json();

      if(response.status !== 200) throw result;

      return result; 
      
    } catch (error) {
      throw error;
    }
  }

  async updateCourse(accesToken, idCourse, data) {
    try {
      const formData = new FormData();
      Object.keys(data).forEach((key) => {
        formData.append(key, data[key])
      });

      if(data.file){
        formData.append("miniature", data.file);
      }

      const url = `${this.baseApi}/${ENV.API_ROUTES.COURSE}/${idCourse}`;
      const params = {
        method: "PATCH",
        headers: {
          Autorization: `Bearer ${accesToken}`
        },
        body: formData
      };

      const response = await fetch(url, params);
      const result = await response.json();

      if(response.status !== 200) throw result;

      return result; 

    } catch (error) {
      throw error;
    }
  }

  async deleteCourse(accessToken, idCourse) {
    try {
      const url = `${this.baseApi}/${ENV.API_ROUTES.COURSE}/${idCourse}`;
      const params = {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Autorization: `Bearer ${accessToken}`
        }
      };

      const response = await fetch(url, params);
      const result = await response.json();

      if(response.status !== 200) throw result;

      return result;

    } catch (error) {
      throw error;
    }
  }


}