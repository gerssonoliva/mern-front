import { useContext, usecontext } from 'react';
import { AuthContext } from "../contexts"

export const useAuth = () => useContext(AuthContext);