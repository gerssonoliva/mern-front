import React from 'react';
import { ListPost } from '../../../components/Web/Blog';
import { Container } from "semantic-ui-react";

export function Blog() {
  return (
    <Container>
      <ListPost />
    </Container>
  )
}
